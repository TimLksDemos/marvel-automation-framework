@test
Feature: Marvel Characters

  Scenario: Validating teh number of returned results
    Given the user gets the url for the character with id "1010699"
    When the user goes to the character page
    Then there will be 14 results
