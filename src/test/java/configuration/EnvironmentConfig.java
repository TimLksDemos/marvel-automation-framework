package configuration;

import java.io.File;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class EnvironmentConfig extends ConfigurationManager {

	private Config config;
	private String baseUrl;
	private String basePath;
	private String publicKey;
	private String privateKey;
	private File configFile;

	public EnvironmentConfig() {
		this.configFile = getConifgFile();
		loadConfigFile();
		loadConfiguration();
	}

	public void loadConfigFile() {
		config = ConfigFactory.parseFile(configFile).getConfig("vdiLocal").resolve();
	}

	@Override
	public void loadConfiguration() {
		baseUrl = config.getString("base-url");
		basePath = config.getString("base-path");
		publicKey = config.getString("public-key");
		privateKey = config.getString("private-key");
	}

	/**
	 * @return the base uri
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @return the base path
	 */
	public String getBasePath() {
		return basePath;
	}

	/**
	 * @return the public key
	 */
	public String getPublicKey() {
		return publicKey;
	}

	/**
	 * @return the private key
	 */
	public String getPrivateKey() {
		return privateKey;
	}

}
