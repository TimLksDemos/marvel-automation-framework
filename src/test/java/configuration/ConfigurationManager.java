package configuration;

import java.io.File;
import java.io.FileNotFoundException;

public abstract class ConfigurationManager {
	
	protected final String mainConfiFilePath = "Config/environments.conf";
	
	public abstract void loadConfiguration();
	
	public File getConifgFile() {
		
		String configuredPath = System.getenv("testConfig");
		
		if (configuredPath == null) {
			return new File(Thread.currentThread().getContextClassLoader().getResource(mainConfiFilePath).getFile());
		} else {
			File configuredFile = new File(configuredPath + "/environments.conf");
			
			if (configuredFile.exists()) {
				return configuredFile;
			} else {
				try {
					throw new FileNotFoundException("File is not found under the given path");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
