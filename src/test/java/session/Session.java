package session;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Component;

import configuration.EnvironmentConfig;

/**
 * 
 * @author timol
 *
 *         Session Class
 */
@Component
public class Session {

	private EnvironmentConfig configuration;
	private WebDriver driver;

	public Session() {
		configuration = new EnvironmentConfig();
		driver = new ChromeDriver();
	}

	/**
	 * @return the base url
	 */
	public String getBaseUrl() {
		return configuration.getBaseUrl();
	}

	/**
	 * @return the base path
	 */
	public String getBasePath() {
		return configuration.getBasePath();
	}

	/**
	 * @return the public key
	 */
	public String getPublicKey() {

		String publicKey = System.getProperty("publicKey");

		if (publicKey == null) {
			return configuration.getPublicKey();
		}

		return publicKey;
	}

	/**
	 * @return the private ley
	 */
	public String getPrivateKey() {

		String privateKey = System.getProperty("privateKey");

		if (privateKey == null) {
			return configuration.getPrivateKey();
		}

		return privateKey;
	}

	/**
	 * Getter for the WebDriver instance associated with this session
	 * 
	 * @return the current web driver instance
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * Quit the WebDriver instance associated with this session
	 * 
	 * @return the current web driver instance
	 */
	public void stopDriver() {
		driver.close();
		driver.quit();
	}

}
