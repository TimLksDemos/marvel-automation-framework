package session;

public class SharedDataKeys {

	// Request related
	public static final String TIMESTAMP = "ts";
	public static final String APIKEY = "apikey";
	public static final String HASH = "hash";
	public static final String LIMIT = "limit";
	public static final String OFFSET = "offset";
	public static final String ORDERBY = "orderBy";
	public static final String SERIES = "series";
	public static final String STARTWITH = "nameStartsWith";
}
