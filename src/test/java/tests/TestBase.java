package tests;

import static org.junit.Assert.fail;

import java.net.MalformedURLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import session.Session;
import session.SessionSingleton;

public class TestBase {

	protected static Session session;
	protected static RequestSpecification requestSpec;

	public static void StepsInit() {
		try {
			session = SessionSingleton.getSession();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			fail("Unable to initialise session: " + e.getMessage());
		}
	}

	@BeforeClass
	public static void initialise() {
		StepsInit();
		requestSpec = createDefaultRequestSpec();
	}

	@AfterClass
	public static void tearDown() {
		session.stopDriver();
	}

	/**
	 * Builds the request specification
	 * 
	 * @return the request specification
	 */
	private static RequestSpecification createDefaultRequestSpec() {

		return new RequestSpecBuilder().setBaseUri(session.getBaseUrl()).setBasePath(session.getBasePath()).build();
	}
}
