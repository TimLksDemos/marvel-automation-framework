package tests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static session.SharedDataKeys.*;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import api.CharactersAPIObject;
import dataobjetcs.Credentials;
import dataobjetcs.ErrorResponse;
import dataobjetcs.Response;
import helpers.CredentialBuilder;
import helpers.ErrorHelper;
import helpers.RandomValuesGenerator;
import io.restassured.http.ContentType;

/**
 * 
 * @author timol
 *
 *         Contains tests for characters
 */
public class CharactersTest extends TestBase {

	/**
	 * Scenario: Successful retrieval of characters
	 * Given the user provides valid information
	 * When a GET request is made
	 * Then a 200 response code is returned
	 * And the characters are returned
	 */
	@Test
	public void get_200_with_valid_information() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		//@formatter:off
		Response reponse = 
			given()
				.spec(requestSpec)
				.params(TIMESTAMP, credentials.getTimestamp())
				.params(APIKEY, credentials.getApiKey())
				.params(HASH, credentials.getHash())
				.contentType(ContentType.JSON)
			.when()
				.get(CharactersAPIObject.charactersURL())
			.then()
				.assertThat().statusCode(200)
				.extract().body().as(Response.class);
		//@formatter:on

		int numberOfCharacter = reponse.getData().getTotal();
		int offset = 0;

		while (offset < numberOfCharacter) {
			//@formatter:off
			given()
				.spec(requestSpec)
				.params(TIMESTAMP, credentials.getTimestamp())
				.params(APIKEY, credentials.getApiKey())
				.params(HASH, credentials.getHash())
				.params(LIMIT, 100)
				.params(OFFSET, offset)
				.contentType(ContentType.JSON)
			.when()
				.get(CharactersAPIObject.charactersURL())
			.then()
				.assertThat().statusCode(200)
				.assertThat().body("data.results.any { it.containsKey('id') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('name') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('description') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('modified') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('thumbnail') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('resourceURI') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('comics') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('series') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('stories') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('events') }", is(true))
				.assertThat().body("data.results.any { it.containsKey('urls') }", is(true));
			//@formatter:on

			offset = offset += 100;
		}
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters using invalid api key
	 * Given the user provides an invalid api key
	 * When a GET request is made
	 * Then a 401 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_401_with_invalid_api_key() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.params(TIMESTAMP, credentials.getTimestamp())
				.params(APIKEY, RandomValuesGenerator.randomString(10, 15))
				.params(HASH, credentials.getHash())
				.contentType(ContentType.JSON)
			.when()
				.get(CharactersAPIObject.charactersURL())
			.then()
				.assertThat().statusCode(401)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getCode()).isEqualTo(ErrorHelper.INVALID_CREDENTIALS_CODE);
		softly.assertThat(errorResponse.getMessage()).isEqualTo(ErrorHelper.INVALID_API_KEY_MESSAGE);
		softly.assertAll();
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters using invalid hash
	 * Given the user provides an invalid hash
	 * When a GET request is made
	 * Then a 401 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_401_with_invalid_hash() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.params(TIMESTAMP, credentials.getTimestamp())
				.params(APIKEY, credentials.getApiKey())
				.params(HASH, RandomValuesGenerator.randomString(10, 15))
				.contentType(ContentType.JSON)
			.when()
				.get(CharactersAPIObject.charactersURL())
			.then()
				.assertThat().statusCode(401)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getCode()).isEqualTo(ErrorHelper.INVALID_CREDENTIALS_CODE);
		softly.assertThat(errorResponse.getMessage()).isEqualTo(ErrorHelper.INVALID_COMBINATION_MESSAGE);
		softly.assertAll();
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters using invalid timestamp
	 * Given provides an invalid timestamp
	 * When a GET request is made
	 * Then a 401 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_401_with_invalid_timestamp() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.params(TIMESTAMP, RandomValuesGenerator.randomString(1, 2))
				.params(APIKEY, credentials.getApiKey())
				.params(HASH, credentials.getHash())
				.contentType(ContentType.JSON)
			.when()
				.get(CharactersAPIObject.charactersURL())
			.then()
				.assertThat().statusCode(401)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getCode()).isEqualTo(ErrorHelper.INVALID_CREDENTIALS_CODE);
		softly.assertThat(errorResponse.getMessage()).isEqualTo(ErrorHelper.INVALID_COMBINATION_MESSAGE);
		softly.assertAll();
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with no api key
	 * Given the user provides no api key
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_no_api_key() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.params(TIMESTAMP, credentials.getTimestamp())
				.params(HASH, credentials.getHash())
				.contentType(ContentType.JSON)
			.when()
				.get(CharactersAPIObject.charactersURL())
			.then()
				.assertThat().statusCode(409)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getCode()).isEqualTo(ErrorHelper.MISSING_PARAMETER_CODE);
		softly.assertThat(errorResponse.getMessage()).isEqualTo(ErrorHelper.MISSING_API_KEY_MESSAGE);
		softly.assertAll();
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with no hash
	 * Given the user provides no hash
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_no_hash() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.params(TIMESTAMP, credentials.getTimestamp())
				.params(APIKEY, credentials.getApiKey())
				.contentType(ContentType.JSON)
			.when()
				.get(CharactersAPIObject.charactersURL())
			.then()
				.assertThat().statusCode(409)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getCode()).isEqualTo(ErrorHelper.MISSING_PARAMETER_CODE);
		softly.assertThat(errorResponse.getMessage()).isEqualTo(ErrorHelper.MISSING_HASH_MESSAGE);
		softly.assertAll();
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with no timestamp
	 * Given provides no timestamp
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_no_timestamp() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		//@formatter:off
		ErrorResponse errorResponse = 
			given()
				.spec(requestSpec)
				.params(APIKEY, credentials.getApiKey())
				.params(HASH, credentials.getHash())
				.contentType(ContentType.JSON)
			.when()
				.get(CharactersAPIObject.charactersURL())
			.then()
				.assertThat().statusCode(409)
				.extract().body().as(ErrorResponse.class);
		//@formatter:on

		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(errorResponse.getCode()).isEqualTo(ErrorHelper.MISSING_PARAMETER_CODE);
		softly.assertThat(errorResponse.getMessage()).isEqualTo(ErrorHelper.MISSING_TIMESTAMP_MESSAGE);
		softly.assertAll();
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with invalid parameter
	 * Given provides an invalid parameter
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_invalid_parameter() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		String invalidParameter = RandomValuesGenerator.randomString(5, 15);
		int expectedErrorCode = 409;

		//@formatter:off
		given()
			.spec(requestSpec)
			.params(TIMESTAMP, credentials.getTimestamp())
			.params(APIKEY, credentials.getApiKey())
			.params(HASH, credentials.getHash())
			.params(invalidParameter, RandomValuesGenerator.randomString(5, 15))
			.contentType(ContentType.JSON)
		.when()
			.get(CharactersAPIObject.charactersURL())
		.then()
			.assertThat().statusCode(expectedErrorCode)
			.assertThat().body("code", equalTo(expectedErrorCode))
			.assertThat().body("status", equalTo(String.format("We don't recognize the parameter %s", invalidParameter)));
		//@formatter:on
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with limit set to zero
	 * Given provides a limit set to zero
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_limit_to_0() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		int expectedErrorCode = 409;

		//@formatter:off
		given()
			.spec(requestSpec)
			.params(TIMESTAMP, credentials.getTimestamp())
			.params(APIKEY, credentials.getApiKey())
			.params(HASH, credentials.getHash())
			.params(LIMIT, 0)
			.contentType(ContentType.JSON)
		.when()
			.get(CharactersAPIObject.charactersURL())
		.then()
			.assertThat().statusCode(expectedErrorCode)
			.assertThat().body("code", equalTo(expectedErrorCode))
			.assertThat().body("status", equalTo(ErrorHelper.BELOW_LIMIT_MESSAGE));
		//@formatter:on
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with negative limit
	 * Given provides a negative limit
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_negative_limit() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		int expectedErrorCode = 409;

		//@formatter:off
		given()
			.spec(requestSpec)
			.params(TIMESTAMP, credentials.getTimestamp())
			.params(APIKEY, credentials.getApiKey())
			.params(HASH, credentials.getHash())
			.params(LIMIT, -1)
			.contentType(ContentType.JSON)
		.when()
			.get(CharactersAPIObject.charactersURL())
		.then()
			.assertThat().statusCode(expectedErrorCode)
			.assertThat().body("code", equalTo(expectedErrorCode))
			.assertThat().body("status", equalTo(ErrorHelper.BELOW_LIMIT_MESSAGE));
		//@formatter:on
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with limit above 100
	 * Given provides a limit above 100
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_limit_above_100() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		int expectedErrorCode = 409;

		//@formatter:off
		given()
			.spec(requestSpec)
			.params(TIMESTAMP, credentials.getTimestamp())
			.params(APIKEY, credentials.getApiKey())
			.params(HASH, credentials.getHash())
			.params(LIMIT, 101)
			.contentType(ContentType.JSON)
		.when()
			.get(CharactersAPIObject.charactersURL())
		.then()
			.assertThat().statusCode(expectedErrorCode)
			.assertThat().body("code", equalTo(expectedErrorCode))
			.assertThat().body("status", equalTo(ErrorHelper.ABOVE_LIMIT_MESSAGE));
		//@formatter:on
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with invalid ordering
	 * Given provides an invalid ordering
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_invalid_ordering() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		String invalidOrderBy = RandomValuesGenerator.randomString(10, 15);
		int expectedErrorCode = 409;

		//@formatter:off
		given()
			.spec(requestSpec)
			.params(TIMESTAMP, credentials.getTimestamp())
			.params(APIKEY, credentials.getApiKey())
			.params(HASH, credentials.getHash())
			.params(ORDERBY, invalidOrderBy)
			.contentType(ContentType.JSON)
		.when()
			.get(CharactersAPIObject.charactersURL())
		.then()
			.assertThat().statusCode(expectedErrorCode)
			.assertThat().body("code", equalTo(expectedErrorCode))
			.assertThat().body("status", equalTo(String.format("%s is not a valid ordering parameter.", invalidOrderBy)));
		//@formatter:on
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with no filter
	 * Given provides no filter
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_no_filter() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		int expectedErrorCode = 409;

		//@formatter:off
		given()
			.spec(requestSpec)
			.params(TIMESTAMP, credentials.getTimestamp())
			.params(APIKEY, credentials.getApiKey())
			.params(HASH, credentials.getHash())
			.params(SERIES, "")
			.contentType(ContentType.JSON)
		.when()
			.get(CharactersAPIObject.charactersURL())
		.then()
			.assertThat().statusCode(expectedErrorCode)
			.assertThat().body("code", equalTo(expectedErrorCode))
			.assertThat().body("status", equalTo(ErrorHelper.FILTER_ERROR_MESSAGE));
		//@formatter:on
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with invalid filter
	 * Given provides an invalid filter
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_invalid_filter() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		int expectedErrorCode = 409;

		//@formatter:off
		given()
			.spec(requestSpec)
			.params(TIMESTAMP, credentials.getTimestamp())
			.params(APIKEY, credentials.getApiKey())
			.params(HASH, credentials.getHash())
			.params(SERIES, RandomValuesGenerator.randomString(10, 15))
			.contentType(ContentType.JSON)
		.when()
			.get(CharactersAPIObject.charactersURL())
		.then()
			.assertThat().statusCode(expectedErrorCode)
			.assertThat().body("code", equalTo(expectedErrorCode))
			.assertThat().body("status", equalTo(ErrorHelper.FILTER_ERROR_MESSAGE));
		//@formatter:on
	}

	/**
	 * Scenario: Unsuccessful retrieval of characters with no start with
	 * Given provides no start with
	 * When a GET request is made
	 * Then a 409 response code is returned
	 * And the appropriate error response is returned
	 */
	@Test
	public void get_409_with_no_start_with() throws JsonProcessingException {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		int expectedErrorCode = 409;

		//@formatter:off
		given()
			.spec(requestSpec)
			.params(TIMESTAMP, credentials.getTimestamp())
			.params(APIKEY, credentials.getApiKey())
			.params(HASH, credentials.getHash())
			.params(STARTWITH, "")
			.contentType(ContentType.JSON)
		.when()
			.get(CharactersAPIObject.charactersURL())
		.then()
			.assertThat().statusCode(expectedErrorCode)
			.assertThat().body("code", equalTo(expectedErrorCode))
			.assertThat().body("status", equalTo(ErrorHelper.NO_START_WITH_MESSAGE));
		//@formatter:on
	}

}
