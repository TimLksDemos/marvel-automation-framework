package steps;

import static org.assertj.core.api.Assertions.assertThat;
import static session.SharedDataKeys.APIKEY;
import static session.SharedDataKeys.HASH;
import static session.SharedDataKeys.TIMESTAMP;

import java.net.MalformedURLException;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import api.CharactersAPIObject;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dataobjetcs.Credentials;
import dataobjetcs.Response;
import helpers.CredentialBuilder;
import pageobjects.ResultOptionsComponent;

/**
 * 
 * @author timol
 *
 *         Step definitions for characters feature
 */
public class CharacterSteps extends StepBase {

	private ResultOptionsComponent resultOptionsComponent;
	private int characterAvailableComics = 0;
	private String characterPageUrl = "";

	@Before
	public void setUp() {
		resultOptionsComponent = new ResultOptionsComponent(session.getDriver());
	}

	@After
	public void tearDown() throws MalformedURLException {
		session.stopDriver();
	}

	@Given("^the user gets the url for the character with id \"([^\"]*)\"$")
	public void the_user_gets_the_url_for_the_character_with_id(String id) throws Throwable {

		Credentials credentials = CredentialBuilder.getCredentials().withTimestamp("1")
				.withPrivateKey(session.getPrivateKey()).withPublicKey(session.getPublicKey()).build();

		HttpClient httpclient = HttpClientBuilder.create().build();

		//@formatter:off
		URI uri = new URIBuilder()
				.setScheme("http")
	            .setHost("gateway.marvel.com")
	            .setPath(session.getBasePath()+CharactersAPIObject.singleCharacterURL(id))
	            .setParameter(TIMESTAMP, credentials.getTimestamp())
	            .setParameter(APIKEY, credentials.getApiKey())
	            .setParameter(HASH, credentials.getHash())
	            .build();
		//@formatter:on

		HttpGet request = new HttpGet(uri);

		HttpResponse httpResponse = httpclient.execute(request);

		ObjectMapper mapper = new ObjectMapper();
		Response response = mapper.readValue(httpResponse.getEntity().getContent(), Response.class);

		characterAvailableComics = response.getData().getResults().get(0).getComics().getAvailable();
		characterPageUrl = response.getData().getResults().get(0).getUrls().get(0).getUrl();
	}

	@When("^the user goes to the character page$")
	public void the_user_goes_to_the_character_page() throws Throwable {
		session.getDriver().navigate().to(characterPageUrl);
	}

	@Then("^there will be (\\d+) results$")
	public void there_will_be_results(int results) throws Throwable {
		assertThat(resultOptionsComponent.resultsText()).contains(Integer.toString(characterAvailableComics));
	}
}
