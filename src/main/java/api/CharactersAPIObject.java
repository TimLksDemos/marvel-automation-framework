package api;

/**
 * 
 * @author timol
 *
 *         Contains methods for characters
 */
public class CharactersAPIObject {

	/**
	 * Provides the url for characters
	 * 
	 * @return the URL for characters
	 */
	public static String charactersURL() {
		return "characters";
	}

	/**
	 * Provides the url for a single character
	 * 
	 * @param id
	 *           The id of the character
	 * 
	 * @return the URL for character
	 */
	public static String singleCharacterURL(String id) {
		return String.format("characters/%s", id);
	}
}
