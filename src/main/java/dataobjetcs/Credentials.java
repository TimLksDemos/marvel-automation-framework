package dataobjetcs;

/**
 * 
 * @author timol
 *
 *         POJO for user credentials
 */
public class Credentials {

	private String timestamp;
	private String apiKey;
	private String hash;

	public Credentials() {
		super();
	}

	public Credentials(String timestamp, String apiKey, String hash) {
		super();
		this.timestamp = timestamp;
		this.apiKey = apiKey;
		this.hash = hash;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
