package dataobjetcs;

import java.util.List;

/**
 * 
 * @author timol
 *
 *         POJO for comic
 */
public class Comic {

	private int available;
	private String collectionURI;
	private List<Object> items;
	private int returned;

	public Comic() {
		super();
	}

	public Comic(int available, String collectionURI, List<Object> items, int returned) {
		super();
		this.available = available;
		this.collectionURI = collectionURI;
		this.items = items;
		this.returned = returned;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public String getCollectionURI() {
		return collectionURI;
	}

	public void setCollectionURI(String collectionURI) {
		this.collectionURI = collectionURI;
	}

	public List<Object> getItems() {
		return items;
	}

	public void setItems(List<Object> items) {
		this.items = items;
	}

	public int getReturned() {
		return returned;
	}

	public void setReturned(int returned) {
		this.returned = returned;
	}

}
