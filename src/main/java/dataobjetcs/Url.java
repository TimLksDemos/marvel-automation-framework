package dataobjetcs;

/**
 * 
 * @author timol
 *
 *         POJO for url
 */
public class Url {

	private String type;
	private String url;

	public Url() {
		super();
	}

	public Url(String type, String url) {
		super();
		this.type = type;
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
