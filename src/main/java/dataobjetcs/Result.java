package dataobjetcs;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author timol
 *
 *         POJO for result
 */
public class Result {

	private int id;
	private String name;
	private String description;
	private String modified;
	private Map<String, Object> thumbnail;
	private String resourceURI;
	private Comic comics;
	private Map<String, Object> series;
	private Map<String, Object> stories;
	private Map<String, Object> events;
	private List<Url> urls;

	public Result() {
		super();
	}

	public Result(int id, String name, String description, String modified, Map<String, Object> thumbnail,
			String resourceURI, Comic comics, Map<String, Object> series, Map<String, Object> stories,
			Map<String, Object> events, List<Url> urls) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.modified = modified;
		this.thumbnail = thumbnail;
		this.resourceURI = resourceURI;
		this.comics = comics;
		this.series = series;
		this.stories = stories;
		this.events = events;
		this.urls = urls;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Map<String, Object> getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Map<String, Object> thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public Comic getComics() {
		return comics;
	}

	public void setComics(Comic comics) {
		this.comics = comics;
	}

	public Map<String, Object> getSeries() {
		return series;
	}

	public void setSeries(Map<String, Object> series) {
		this.series = series;
	}

	public Map<String, Object> getStories() {
		return stories;
	}

	public void setStories(Map<String, Object> stories) {
		this.stories = stories;
	}

	public Map<String, Object> getEvents() {
		return events;
	}

	public void setEvents(Map<String, Object> events) {
		this.events = events;
	}

	public List<Url> getUrls() {
		return urls;
	}

	public void setUrls(List<Url> urls) {
		this.urls = urls;
	}

}
