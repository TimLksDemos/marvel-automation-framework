package helpers;

/**
 * 
 * @author timol
 *
 *         Contains error codes and messages
 */
public class ErrorHelper {

	// Error Codes

	public static String INVALID_CREDENTIALS_CODE = "InvalidCredentials";
	public static String MISSING_PARAMETER_CODE = "MissingParameter";

	// Error Messages

	public static String INVALID_COMBINATION_MESSAGE = "That hash, timestamp and key combination is invalid.";
	public static String INVALID_API_KEY_MESSAGE = "The passed API key is invalid.";
	public static String MISSING_API_KEY_MESSAGE = "You must provide a user key.";
	public static String MISSING_HASH_MESSAGE = "You must provide a hash.";
	public static String MISSING_TIMESTAMP_MESSAGE = "You must provide a timestamp.";
	public static String ABOVE_LIMIT_MESSAGE = "You may not request more than 100 items.";
	public static String BELOW_LIMIT_MESSAGE = "You must pass an integer limit greater than 0.";
	public static String FILTER_ERROR_MESSAGE = "You must pass at least one valid series if you set the series filter.";
	public static String NO_START_WITH_MESSAGE = "nameStartsWith cannot be blank if it is set";
}
