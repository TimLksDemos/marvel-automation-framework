package helpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import dataobjetcs.Credentials;

/**
 * 
 * @author timol
 *
 *         Builder class to create a Credential object
 */
public class CredentialBuilder {

	private String timestamp;
	private String publicKey;
	private String privateKey;

	/**
	 * @return a new instance of the class
	 */
	public static CredentialBuilder getCredentials() {
		return new CredentialBuilder();
	}

	/**
	 * 
	 * @param timestamp
	 *                  A timestamp
	 * 
	 * @return an instance of the class
	 */
	public CredentialBuilder withTimestamp(String timestamp) {
		this.timestamp = timestamp;
		return this;
	}

	/**
	 * 
	 * @param publicKey
	 *                  The user's public key
	 * 
	 * @return an instance of the class
	 */
	public CredentialBuilder withPublicKey(String publicKey) {
		this.publicKey = publicKey;
		return this;
	}

	/**
	 * 
	 * @param privateKey
	 *                   The user's private key
	 * 
	 * @return an instance of the class
	 */
	public CredentialBuilder withPrivateKey(String privateKey) {
		this.privateKey = privateKey;
		return this;
	}

	/**
	 * Creates a Credential object
	 * 
	 * @return a Credential object
	 */
	public Credentials build() {

		String hashString = timestamp + privateKey + publicKey;
		String hash = "";

		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");

			md.update(hashString.getBytes());
			byte[] digest = md.digest();
			hash = DatatypeConverter.printHexBinary(digest).toLowerCase();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return new Credentials(timestamp, publicKey, hash);
	}

}
