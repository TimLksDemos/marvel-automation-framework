package helpers;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * 
 * @author timol
 *
 *         Contains methods that help generating random values
 */
public class RandomValuesGenerator {

	/**
	 * Creates a random generated String
	 * 
	 * @return a random String
	 */
	public static String randomString(int min, int max) {
		return RandomStringUtils.randomAlphanumeric(randomInt(min, max)).toLowerCase();
	}

	public static int randomInt(int min, int max) {
		Random random = new Random();
		return random.nextInt(max + 1 - min) + min;
	}

}
