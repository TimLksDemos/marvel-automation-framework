package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author timol
 *
 *         Page Object that represent the comic filtering option
 */
public class ResultOptionsComponent {
	private WebDriver driver;

	@FindBy(xpath = "/html/body/div[2]/div/div[1]/div[4]/div/span")
	WebElement results;

	public ResultOptionsComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public String resultsText() {
		return results.getText();
	}
}
