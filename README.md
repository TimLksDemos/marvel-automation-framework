# Coding Challenge - Marvel API - v1
# By Timothy Loukas
#
#
# Prerequisites:
# - Have maven installed and configured
# - Download chromedriver and have its path added to your PATH
# - Download and install Chrome version 72 (preferably)
# - Download and install Eclipse IDE
#
#
# How to build:
# - Open commend line
# - Go to the project directory
# - Run the command: mvn clean install -DskipTests
#
#
# How to run:
# - Open commend line
# - Go to the project directory
# - Run the command: mvn test
#
#
# Additional Arguments:
# When running the test command, you can add 2 additional arguments
# - privateKey
# - publicKey
# Each argument must be added with -D 
# Example -DprivateKey=yourKey -DpublicKey=yourKey
# If no arguments added, the keys will be read from the config
#
#
# Configuration: 
# You can either use the configuration from the projects test recources
# or you can create and use your own congiguration
# The name of the configuration file must be "environments.conf"
# And you must add its path to your system variables
#
#
# Public and Private Keys:
# You can either add them through the command arguments
# or through configuration
# In the existing configuration which is located in the test resources,
# you can find 2 fields (public-key, private-key), which you need to fill out

